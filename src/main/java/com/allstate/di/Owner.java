package com.allstate.di;

public class Owner {

    private int id;
    private String name;
    private Pet pet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Owner(Pet pet) {
        this.pet = pet;
       
    }

    public Owner() {
        this.id = 1;
       
    }
    
}
