package com.allstate.di;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.*;
import org.springframework.beans.factory.annotation.Autowired;
 
@Configuration
 public class PetConfigurer {

    @Bean
    public Owner owner(@Autowired Pet pet){
        return new Owner(pet);
    }

    @Bean
    public Pet pet()
    {
        return new Cat();
    }
}
