package com.allstate.entities;

public class Person {

    private int id;
    private String name;
    private int age;

    //quiz , sealing a class and  cannot change the vaule
    private final static int ageLimit ;
    
    static{
        ageLimit = 100;
    }

    public Person()
    {
        this.id = 0;
        this.name = "sss";
    }

    
    public int getId()
    {
        return this.id;

    }
    
    public void setid(int id)
    {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void display()
    {
        System.out.printf("Values are %s , %s \n",this.name,this.id);
    }

    protected void print()
    {
        System.out.printf("printed Values are %s , %s \n",this.name,this.id);
    }

     void printf()
    {
        System.out.printf("formatted Printed Values are %s , %s \n",this.name,this.id);
    }
    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public static int getAgeLimit()
    {
        return ageLimit;
    }
}
