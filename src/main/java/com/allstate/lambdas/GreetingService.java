package com.allstate.lambdas;

public interface GreetingService {
    
    void sayMessage(String message);
}
