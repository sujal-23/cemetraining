package com.allstate.lambdas;

public class GreetingServiceImpl implements GreetingService{

    @Override
    public void sayMessage(String message) {
        
        System.out.printf("Hi Lambda example %s", message);

    }
    
}
