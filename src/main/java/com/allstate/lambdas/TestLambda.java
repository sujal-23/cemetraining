package com.allstate.lambdas;

public class TestLambda {

    public  static void main(String[] args) {
        GreetingService greeting = (s) -> {
            System.out.printf("Hello Lambda from %s", s);
        };

        greeting.sayMessage("Sujal");
        
    }
    
}
