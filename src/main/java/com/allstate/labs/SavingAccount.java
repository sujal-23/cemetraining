package com.allstate.labs;

public class SavingAccount extends Account {

    public SavingAccount(double balance, String name) {
        super(balance,name);
    }

    @Override
    public void addInterest()
{
    double bal = getBalance();
    bal = bal + (bal/100) * 10;
    setBalance(bal * 1.1);
}
    
}
