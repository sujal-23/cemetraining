package com.allstate.labs;

public class CurrentAccount extends Account {

    public CurrentAccount(double balance, String name) {
        super(balance,name);
    }

    @Override
    public void addInterest()
    {
    double bal = getBalance();
    bal = bal + (bal/100) * 10;
    setBalance(bal * 2.1);
    }
    
}
