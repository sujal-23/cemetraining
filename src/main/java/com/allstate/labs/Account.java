package com.allstate.labs;

public abstract class Account {
    private double balance;
    private String name;
    private static int accountInterestRate = 10;
    private final static int maxAmount = 20;

    public boolean withdraw()
    {
      return withdraw(20.00);
    }

    public boolean withdraw(double amount)
    {
        if( this.balance > amount && this.balance > maxAmount)
        {
            this.balance = this.balance - amount;
            System.out.printf("have enough value %s \n", "true");
            return true;
        }
        System.out.printf("have enough value %s \n", "false");
        return false;
    }

    public static int getInterestRate()
    {
        return accountInterestRate;
    }

    public static void setInterestRate(int newRate)
    {
        accountInterestRate = newRate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void display()
    {
        System.out.printf("Values are %s , %s \n",this.name,this.balance);
    }

// public void addInterest()
// {
//     double bal = getBalance();
//     bal = bal + (bal/100) * 10;
//     setBalance(bal);
// }

public abstract void addInterest();

public Account(double balance, String name) {
    this.balance = balance;
    this.name = name;
}

public Account()
{
this.name = "Sujal";
this.balance = 200;
}    
}
