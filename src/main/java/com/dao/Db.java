package com.dao;
import com.allstate.entities.*;
import java.util.*;

public interface Db {
    int count();
    Employee find(int id);
    List<Employee> findAll();
    
}
